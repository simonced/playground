/*
This is a simple nodejs script
that watches for new files in current directory 
and rename them with a number in front of them.

Useful when making screenshots automatically saved from 
"greenshot" or the chrome extension "full page screen capture".

To adapt that script to other situations, simply change the
file patterns "^\d{2}_screencapture" (for counting existing renamed files)
and screencapture*.png (glob) on the last line.

This script will watch new files matching the glob untill killed.
Kill script with Ctrl-C in the terminal.
*/

// tools
const util = require("util");
const fs = require("fs");
const path = require("path");
const os = require('os');
// external lib
var chokidar = require("chokidar");

// global state
var last_screen_num = 1;
var watchFolder = os.homedir()+"/Desktop/";

fs.readdirSync(watchFolder).forEach(file => {
	if(/^\d{2}_screencapture/.test(file)) {
		++ last_screen_num;
	}
});


// count already existing files
console.log("Next file number: ", last_screen_num);


// === WATCHER EVENTS ===

// initialize watcher
var watcher = chokidar.watch(watchFolder, {
	ignored: /[\/\\]\./,
	persistent: true,
	ignoreInitial:true // super iimportant!
});


// rename added files
watcher.on('add', (newFile) => {
	var filePath = path.dirname(newFile);
	var fileName = path.basename(newFile);
	
	if( !/^screencapture.*\.png$/.test(fileName) ) {
		// not matching what we are looking for
		//console.log("we ignore: ", fileName);
		return;
	}
	console.log("File added:", fileName);

	// rename only file name
	var newFileName = ("0" + last_screen_num).substr(-2) + "_" + fileName;
	// rename file on disc
	fs.renameSync(newFile, filePath+"/"+newFileName);
	// inscrease files counter
	++last_screen_num;
});