//var socket_server = "192.168.1.109"
//var socket = io.connect('http://'+socket_server+':4200');
var socket = io();

// connect to server
socket.on('connect', function(data) {
	socket.emit('join', 'Hello World from client');
});

// broadcast received from server
socket.on('broad', function(data) {
	$('#output').prepend("<div>"+data+"</div>");
});

// set position of cube
socket.on('cube', function(data) {
	// set position of cube
	$('#cube').css('left', data.x);
	$('#cube').css('top', data.y);
});

// form actions
$('#textsender').on('submit', function(e_) {
	e_.preventDefault();
	console.log( "sending msg to socket!" );

	var msg = $('#input').val();
	// send to socket
	socket.emit('messages', msg);

	// lastly we clear
	$('#input').val("");
});


// moving box ==============================
// drag
$('#cube').on('mousedown', function(e_) {
	var cube = $(this);
	cube.addClass('moving');
	// memo X position
	cube.data('mouse-x', e_.pageX - cube.offset().left);
	cube.data('mouse-y', e_.pageY - cube.offset().top);
});


// drop
$('#cube').on('mouseup', function() {
	$(this).removeClass('moving');
});


// moving!
$(document).on('mousemove', function(e_) {
	var cube = $('#cube');

	if(cube.hasClass('moving')) {
		//console.log( "Moving!", cube.data('mouse-x') );

		var newX = e_.pageX - cube.data('mouse-x');
		var newY = e_.pageY - cube.data('mouse-y');
		cube.css( 'left', newX);
		cube.css( 'top', newY);

		// emmit to socket
		socket.emit('cubemoving', {x: newX, y: newY} );
	}
});

