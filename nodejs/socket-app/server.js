// app.js
var express = require('express');  
var app = express();  
var server = require('http').createServer(app);  
var io = require('socket.io')(server);

const SERVER_PORT = 4200;

// path set for serving jquery + socket.io libs
app.use(express.static(__dirname + '/node_modules'));  
app.use(express.static(__dirname + '/public'));  

// main route
app.get('/', function(req, res,next) {  
    res.sendFile(__dirname + '/index.html');
});

server.listen(SERVER_PORT);
console.log('server listening on port ' + SERVER_PORT);

// socket logic
io.on('connection', function(client) {  
	console.log('Client connected...');

	client.on('join', function(data) {
		console.log("client>",data);
	});

	client.on('messages', function(data) {
		// client itself
		client.emit('broad', data);
		// all other client
		client.broadcast.emit('broad', data);
	});

	// moving cube
	client.on('cubemoving', function(data) {
		client.broadcast.emit('cube', data);
	});
});
